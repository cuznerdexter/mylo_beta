(function(){
    "use strict";
    
    var root = this,
        Chart = root.Chart,
        helpers = Chart.helpers;
    
    var defaultConfig = {

    };
    
    Chart.Type.extend({
        name: "MyloChart",
        defaults: defaultConfig,
        initialize: function(data){
            console.log('mylobar init');
        },
        
        update: function(){
            console.log('mylobar update');
        },
        
        draw: function(){
            console.log('mylobar draw');
        }
    });
}).call(this);