
var myloApp = angular.module('mylo', ['ionic', 'ionic.utils', 'ui.router', 'ngCordova', 'ngAnimate', 'firebase', 'mylo.controllers', 'mylo.directives', 'mylo.services', 'mylo.servicesConnect', 'mylo.servicesData', 'mylo.servicesAuth']);

myloApp.config(['$stateProvider', '$urlRouterProvider', '$ionicConfigProvider',
  function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
      
  $ionicConfigProvider.views.transition('android');
  $ionicConfigProvider.views.maxCache(0); 
  
  $stateProvider

  .state('intro', {
    url: '/intro',
    templateUrl: 'templates/intro.html',
    controller: 'IntroCtrl',
    data: { dragbool: false}
  })
  

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/sideMenuLayout.html',    
    controller: 'AppCtrl'
  })

  .state('app.home', {
    url: '/home',    
    views: {
      'mainContent': {
        templateUrl: 'templates/home.html',
        controller: 'HomeCtrl',
        data: { dragbool: true,
                yepNopePage: false,
                homeState: true,
                colorVal: ['redBg', 'redBg', 'redBg'],
                myloLinks: {
                  newEntry: 1,
                  viewStats: 5
                } 
        }
      }
    }
  })

  
  .state('app.login', {
    url: '/login',
    views: {
      'mainContent': {
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl',
        data: { dragbool: true,
                yepNopePage: false,
                homeState: true,
                colorVal: ['blueBg', 'blueBg', 'blueBg']               
        }
      }
    }
  })


  .state('app.meditated', {
    url: '/meditated',
    views: {
      'mainContent': {
        templateUrl: 'templates/meditated.html',
        controller: 'MeditatedCtrl',
        data: { dragbool: true,
                yepNopePage: true,
                homeState: false,
                colorVal: ['greenBg', 'greenBg', 'redBg'],   
                myloLinks: {
                  medEventYes: 2,
                  medEventNo: 6
                },
                pageSet: 0           
        }
      }
    }
  })
  
  

  .state('app.meditationLength', {
    url: '/meditationLength',    
    views: {      
      'mainContent': {
        templateUrl: 'templates/meditationLength.html',
        controller: 'MeditationLengthCtrl',
        data: { dragbool: true,
                yepNopePage: false,
                homeState: false,
                colorVal: ['greenBg', 'greenBg', 'greenBg'],
                pageSet: 1
                
        }    
      }
    }
  })

  .state('app.meditationThoughtless', {
    url: '/meditationThoughtless',    
    views: {      
      'mainContent': {
        templateUrl: 'templates/meditationThoughtless.html',
        controller: 'MeditationThoughtlessCtrl',
        data: { dragbool: true,
                yepNopePage: true,
                homeState: false,
                colorVal: ['blueBg', 'blueBg', 'purpBg'],
                myloLinks: {
                  medEventYes: 4 ,
                  medEventNo: 6
                },
                pageSet: 2                
        }  
      }  
    }
  })

  .state('app.meditationDay', {
    url: '/meditationDay',    
    views: {      
      'mainContent': {
        templateUrl: 'templates/meditationDay.html',
        controller: 'MeditationDayCtrl',
        data: { dragbool: true,
                yepNopePage: false,
                homeState: false,
                colorVal: ['yellBg', 'greenBg', 'greenBg'],
                pageSet: 1
                
        }    
      }  
    }
  })

  .state('app.meditationStats', {
    url: '/meditationStats',    
    views: {      
      'mainContent': {        
        templateUrl: 'templates/meditationStats.html',
        controller: 'MeditationStatsCtrl',   
        resolve: {
          loadData: function ($timeout, $q) {
            return function () {
              var defer = $q.defer();

              $timeout(function() {
                defer.resolve('data received!');
              }, 2000);

              return defer.promise;
            };
          }
        },   
        data: { dragbool: true,
                yepNopePage: false,
                homeState: false,
                colorVal: ['blueBg', 'blueBg', 'blueBg'],
                myloLinks: {
                  medEventHome: 0,
                  medEventAdvice: 8
                }
        }        
      }
    }    
  })

  .state('app.meditationAdvice', {
    url: '/meditationAdvice',    
    views: {      
      'mainContent': {
        templateUrl: 'templates/meditationAdvice.html',
        controller: 'MeditationAdviceCtrl',
        data: { dragbool: true,
                yepNopePage: false,
                homeState: false,
                colorVal: ['redBg', 'purpBg', 'purpBg'],
                myloLinks: {
                  medEventHome: 0
                }
        }   
      } 
    }    
  })

  .state('app.meditationReflect', {
    url: '/meditationReflect',    
    views: {      
      'mainContent': {
        templateUrl: 'templates/meditationReflect.html',
        controller: 'MeditationReflectCtrl',
        data: { dragbool: true,
                yepNopePage: false,
                homeState: false,
                colorVal: ['purpBg', 'purpBg', 'purpBg']
        }   
      } 
    }    
  })

  .state('app.meditationHow', {
    url: '/meditationHow',    
    views: {      
      'mainContent': {
        templateUrl: 'templates/meditationHow.html',
        controller: 'MeditationHowCtrl',
        data: { dragbool: true,
                yepNopePage: false,
                homeState: true,
                colorVal: ['greenBg', 'purpBg', 'purpBg']
        }   
      } 
    }
  })
  
  .state('app.meditationGuide', {
    url: '/meditationGuide',    
    views: {      
      'mainContent': {
        templateUrl: 'templates/meditationGuide.html',
        controller: 'MeditationGuideCtrl',
        data: { dragbool: true,
                yepNopePage: false,
                homeState: true,
                colorVal: ['greenBg', 'purpBg', 'purpBg']
        }   
      } 
    }
  })

  .state('app.meditationFeedback', {
    url: '/meditationFeedback',    
    views: {      
      'mainContent': {
        templateUrl: 'templates/meditationFeedback.html',
        controller: 'MeditationFeedbackCtrl',
        data: { dragbool: true,
                yepNopePage: false,
                homeState: true,
                colorVal: ['purpBg', 'purpBg', 'purpBg']
        }   
      } 
    }
  })


  .state('app.about', {
    url: '/about',
    views: {
      'mainContent': {
        templateUrl: 'templates/about.html',
        controller: 'AboutCtrl',
        data: { dragbool: true,
                yepNopePage: false,
                homeState: true,
                colorVal: ['purpBg', 'purpBg', 'purpBg']
        },  
        resolve: {
          aboutData: function (dataLoadService) {
            return dataLoadService.getData('js/content.json');
          }
        }
      }
    }
  });
   
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/intro');
}])


.run(['$rootScope', '$firebase', '$firebaseAuth', '$state', 'introCheckService', 'ServicesConnect', 'ServicesAuth', 'ServicesData', '$ionicBackdrop', '$ionicPlatform', '$timeout',
  function ($rootScope, $firebase, $firebaseAuth, $state, introCheckService, ServicesConnect, ServicesAuth, ServicesData, $ionicBackdrop, $ionicPlatform, $timeout) {

    /*
    /   var storing prev page - for back button 
    */
    $rootScope.mylosaved = {};
    $rootScope.myloJson = {};
    

    /*
    /   Check for intro
    */    
    
    if(ServicesAuth.getLoggedInStatus() ){
      introCheckService.curr = false;         
    }else {
      introCheckService.curr = true;      
    }
    if(introCheckService.curr)
    {      
      $state.go('intro');         
    }else {
          $state.go('app.home');           
    }

    

    /*
    /   check prev state name before actually changing page
    /   if nxt state is login  -  set $rootScope.mylosaved.state to page no.
    /   (enables the login page to dynamically return to prev page )
    */
    $rootScope.$on('$stateChangeStart',
    function (event, toState, toParams, fromState, fromParams){        
        console.log("Go to: " + toState.name);
        if(toState.name == 'app.login')
        {
          var stateArr = ['home', 'meditated', 'meditationLength', 'meditationThoughtless', 'meditationStats', 'login', 'meditationReflect'];
          var str = fromState.name;
          if(str !='intro'){           
            if(str == 'undefined' || str == 'null' || str === '')
            {
              str = 'home';
            }else{
              str = str.slice(4); //removes the .app from name
            }            
          }          
          for(var numi =0; numi < stateArr.length; numi++ )
          {
            if(stateArr[numi] == str)
            {
                $rootScope.mylosaved.state = numi;
            }
          }          
        }
        
    });

   
    /* check for connection
          if true get data from firebase
          if false get data from local storage
    */
    ServicesConnect.check();
    

   $ionicPlatform.ready(function() {
    // Initialize push notifications!
    if(window.cordova){
        // first, lets initialize parse. fill in your parse appId and clientKey
      window.parsePlugin.initialize('m13C2OqySDXnKhQx7aWKnYJYp1MBE7idZ5qDcJvj', 'OduoyAGx05MnC1Sj5DzguoeLz1PjN7BPaWcZoeEj', function() {
        
        window.parsePlugin.subscribe('MyloChannel', function() {
         
            window.parsePlugin.getInstallationId(function(id) {
              
                /**
                 * Now you can construct an object and save it to your own services, or Parse, and corrilate users to parse installations
                 * 
                 var install_data = {
                    installation_id: id,
                    channels: ['SampleChannel']
                 }
                 *
                 */

            }, function(e) {
              console.log('Failure to retrieve install id.');
            });
        }, function(e) {
            console.log('Failed trying to subscribe to SampleChannel.');
        });
      }, function(e) {
          console.log('Failure to initialize Parse.');
      });

      
      /*
      *   Initialize local notifications plugin
      */
      window.plugin.notification.local.onadd = function (id, state, json) {
        var notification = {
          id: id,
          state: state,
          json: json 
        };
        $timeout(function () {
          $rootScope.$broadcast("$cordovaLocalNotification:added", notification);
        });
      };
    }
  });
}]);