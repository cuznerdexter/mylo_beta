angular.module('mylo.servicesStats', [])

.factory('ServicesStats', ['$firebase', function ($firebase) { 

	var ServiceStatsInst = {};
	
	ServiceStatsInst.updateLocalObj = function () {
		if (window.localStorage !== undefined || window.localStorage !== null)
		{
			var firebaseRef = new Firebase('https://mylo.firebaseio.com/');
			var firebaseSync = $firebase(firebaseRef.child('users'));
			var firebaseObj = firebaseSync.$asObject();
			return firebaseObj;
		}
	};	

	ServiceStatsInst.localObj = ServiceStatsInst.updateLocalObj();
	
	return ServiceStatsInst;
}]);  
        